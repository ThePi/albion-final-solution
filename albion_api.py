import requests
import math
from datetime import datetime, timedelta
from classes import *
REVERSE_ITEMS_SORT = True

MIN_ITEM_ID = 2150
MAX_ITEM_ID = 3610
VALID_PRICE_MIN = 0
VALID_PRICE_MAX = math.inf
VALID_MARGIN_MIN = 0.05
VALID_MARGIN_MAX = math.inf
ITEMS_PER_REQUEST = 200
ITEMS_DISPLAY_LIMIT = 200

def generateTrades(argsIn):
    argsIn[0] = True
    argsIn[2] = ""

    #itemsToCheck = [[2150,3610],[5271,6119]] #melee, armor
    itemsToCheck = [[2150, 3610], [4301, 6119]]  # melee, armor
    allItemIDs = Item.allItemIDs
    allItems = []
    valuableItems = {}

    setupItems(itemsToCheck, allItems, argsIn)
    doItemTrades(allItems, valuableItems)

    argsIn[2] = ""
    argsIn[2] += "Results are available on the right.\n"
    argsIn[2] += "Showing top {} results in each city.\n".format(ITEMS_DISPLAY_LIMIT)

    sortValuableItems(valuableItems, argsIn[3]["sortmethod"])
    outputItemsToGui(valuableItems, argsIn)

    now = datetime.now().strftime('%m/%d %H:%M:%S')
    argsIn[2] += "Current time is:\n{}".format(now)
    argsIn[0] = False
    argsIn[1] = True


def setupItems(itemsToCheck, allItems, argsIn):
    allItemIDs = Item.allItemIDs
    lastItem = 0
    for itemRange in itemsToCheck:
        for i in range(itemRange[0], itemRange[1]+1, ITEMS_PER_REQUEST):
            it = itemGroup(allItemIDs[i - ITEMS_PER_REQUEST:i], argsIn)
            allItems.append(it)
            lastItem = i

        it = itemGroup(allItemIDs[lastItem:itemRange[1]], argsIn)
        allItems.append(it)
    argsIn[2] += "Finished setting up items\n"

def doItemTrades(allItems, valuableItems):
    for itemBlock in allItems:
        itemBlock.fetchPrices()
        itemBlock.evalProfit()
        for location in list(itemBlock.valuableItems.values()):
            for item in location:
                if item.min_sell_location not in valuableItems:
                    valuableItems[item.min_sell_location] = []
                valuableItems[item.min_sell_location].append(item)

def sortValuableItems(valuableItems, sortMethod = "profit"):
    sortMethodMap = {"profit":Item.getProfit, "bmProfit":Item.getBMProfit}
    itemSortFn = sortMethodMap[sortMethod]
    for cityList in list(valuableItems.values()):
        sortFn = lambda x: itemSortFn(x) if VALID_PRICE_MIN < itemSortFn(x) < VALID_PRICE_MAX else -1
        cityList.sort(key=sortFn, reverse=REVERSE_ITEMS_SORT)

def outputItemsToGui(valuableItems, argsIn):
    for cityList in list(valuableItems.values()):
        for item in cityList[:ITEMS_DISPLAY_LIMIT]:
            if item.min_sell_location not in argsIn[4]:
                argsIn[4][item.min_sell_location] = ""
            argsIn[4][item.min_sell_location] += "{}\n".format(item)


        #also do the JSON part at the end
        for item in cityList[:ITEMS_DISPLAY_LIMIT]:
            if item.min_sell_location not in argsIn[4]:
                argsIn[4][item.min_sell_location] = ""
            argsIn[4][item.min_sell_location] += '{};'.format(item.getDataString())


def grabAveragePrice(idString):
    #baseUrl = "http://localhost:5000/api/v2/stats/history/"
    baseUrl = "http://www.albion-online-data.com/api/v2/stats/history/"
    URL = baseUrl + idString + "?date=&locations=Black Market&qualities=" + "" + "&time-scale=6" #str(quality)
    response = requests.get(URL)
    avgPriceDict = {}
    defaultPriceOrder = [2, 1, 3, 4] #don't count masterpiece as equal
    try:
        requestJSON = response.json()
    except:
        print("Could not fetch URL {}: {}".format(URL, response))
        return avgPriceDict
    if not len(requestJSON) > 0:
        return avgPriceDict
        
    MIN_QUAL = 1
    MAX_QUAL = 5
        
    for qual in range(MIN_QUAL, MAX_QUAL + 1):
        #does correct qual exist?
        qualItemFound = None
        for qualItem in requestJSON:
            if qualItem["quality"] == qual:
                qualItemFound = qualItem
                #print("Found: {}".format(qualItemFound["quality"]))
                break
        
        #else, get nearest.
        if qualItemFound is None:
            qualitiesExist = {}
            for qualItem in requestJSON:
                qualitiesExist[qualItem["quality"]] = qualItem
            
            for po in defaultPriceOrder:
                if po in qualitiesExist:
                    qualItemFound = qualitiesExist[po]
                    #print("{} Taken instead of {}: {}".format(po,qual,qualItemFound["quality"]))
                    break
                    
        #if we still can't find it, it's q5, let's not assume a price then.
        if qualItemFound is None:
            #print("Exiting @q{}, not found".format(qual))
            continue
                
        #calculate the avg price, num times sold.
    
        qFound = qualItemFound["quality"]
        price = 0
        totalNumSeen = 0
        for dataItem in qualItemFound["data"]:
            price += dataItem["avg_price"]
            totalNumSeen += dataItem["item_count"]
            
        price /= len(qualItemFound["data"])
        
        # TODO: Make an estimated sell/day figure instead of total sold.
        # avgSeen = sum(itemResponse["data"]["item_count"]) #FIX: just shows total recorded sells
        avgPriceDict[qual] = (price,totalNumSeen)

    return avgPriceDict

def fetchAveragePrices():
    itemsToCheck = [[2150, 3610], [5271, 6119]]


    numberToID = {}
    averagePrices = {}
    itemsFile = open("items.txt", "r")
    itemsText = itemsFile.readlines()

    for lineNum in range(len(itemsText)):
        parts = itemsText[lineNum].split(":")
        itemID = parts[1].replace(" ", "")
        if len(parts) < 3:
            prettyText = "PLACEHOLDER"
        else:
            prettyText = parts[2].strip()
        numberToID[lineNum] = itemID
    itemsFile.close()

    for itemRange in itemsToCheck:
        for itemNum in range(itemRange[0], itemRange[1]+1):
            itemID = numberToID[itemNum]
            avgPriceDict = grabAveragePrice(itemID)
            for itemPrice in avgPriceDict.items():
                averagePrices[itemID, itemPrice[0]] = itemPrice[1]
    return averagePrices

import json
import requests
from datetime import datetime, timedelta
import pickle
# Define constants for self.price list:
SELL_MIN = 0
SELL_MAX = 1
BUY_MIN = 2
BUY_MAX = 3
LOCATIONS = ["Martlock", "Fort Sterling", "Lymhurst","Thetford","Bridgewatch","Black Market", "Caerleon"]
FILTER_BYPASS = False
#BASE_URL = "http://localhost:5000/api/v2/stats/Prices/"

def save_obj(obj, name):
    with open('obj/' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def loadItemIDs():
    idToName = {}
    allItemIDs = []
    itemsFile = open("items.txt", "r")
    itemsText = itemsFile.readlines()
    for lineNum in range(len(itemsText)):
        parts = itemsText[lineNum].split(":")
        itemID = parts[1].replace(" ", "")
        if len(parts) < 3:
            prettyText = "PLACEHOLDER"
        else:
            prettyText = parts[2].strip()
        idToName[itemID] = prettyText
        allItemIDs.append(itemID)
    itemsFile.close()
    return idToName, allItemIDs

class Item(object):
    blackMarketPrices = load_obj("albiondata_bm_sell")
    defaultTimeString = "0001-01-01T00:00:00"
    idToName, allItemIDs = loadItemIDs()
    auctionFee = .015
    def __init__(self, item_id, quality, argsFromGui):
        self.item_id = item_id
        self.name = Item.getIDFromName(self.item_id)
        self.enchant_level = self.generateEnchantLevel()
        self.quality = quality
        self.profit = None
        self.bmSellProfit = None
        self.bmPrice = self.getBMPrice()
        self.bmVolume = self.getBMVolume()
        self.prices = [0, 0, 0, 0]
        self.dates = [self.defaultTimeString] * 4
        self.min_sell_location = "NA"
        self.max_buy_location = "NA"
        self.sell_lock = argsFromGui["sell_lock"]
        self.buy_lock = argsFromGui["buy_lock"]
        self.sell_filter_lock = argsFromGui["sell_filter_lock"]
        self.sell_time = self.splitFloatHour(argsFromGui["sell_time"])
        self.buy_time = self.splitFloatHour(argsFromGui["buy_time"])
        #sell filter allows you to filter from already profitable trades
        self.margin = -1
        self.tax = argsFromGui["tax"]
        self.base_url = argsFromGui["url"]

    def generateEnchantLevel(self):
        split = self.item_id.split("@")
        if len(split) > 1:
            return split[1]
        else:
            return 0

    @staticmethod
    def getIDFromName(itemID):
        return Item.idToName[itemID]

    @staticmethod
    def splitFloatHour(decimalHour):
        if int(float(decimalHour)) is 0:
            hr = int(float(decimalHour))
            minute = int(float(decimalHour) * 60)
        else:
            hr = int(float(decimalHour))
            minute = int((float(decimalHour) % int(float(decimalHour))) * 60)

        return hr, minute

    def getMargin(self):
        self.margin = ((self.prices[BUY_MAX] // (1+self.tax)) - self.prices[SELL_MIN])/(self.prices[SELL_MIN])
        return self.margin

    def getBMPrice(self):
        bmTuple = (self.item_id, self.quality)
        if bmTuple in self.blackMarketPrices:
            return int(self.blackMarketPrices[bmTuple][0])
        else:
            return -1

    def getBMVolume(self):
        bmTuple = (self.item_id, self.quality)
        if bmTuple in self.blackMarketPrices:
            return int(self.blackMarketPrices[bmTuple][1])
        else:
            return -1

    @staticmethod
    def getProfit(item):
        return item.profit

    @staticmethod
    def getBMProfit(item):
        return item.bmSellProfit

    def __str__(self):
        self.margin = ((self.prices[BUY_MAX] // (1+self.tax)) - self.prices[SELL_MIN])/(self.prices[SELL_MIN])
        out = "----------\n"
        out += "Margin: {:.2%} ".format(self.margin)
        out += "Profit: {:,}, Sell Order: {:,}\n".format(self.profit, self.bmSellProfit)
        out += "{} E{} Q{}: S {:,} ({}) / B {:,} ({})\n".format(self.name, self.enchant_level, self.quality, self.prices[SELL_MIN], self.min_sell_location, self.prices[BUY_MAX], self.max_buy_location)
        out += "BM: {:,}, V:{:,}\n".format(self.bmPrice, self.bmVolume)
        out += "S: {}\n".format(self.dates[SELL_MIN])
        out += "B: {}".format(self.dates[BUY_MAX])
        return out

    def getDataString(self):
        dataDict = {'title':self.name, 'tier':self.extractTierNumber(), 'enchant':self.enchant_level, 'cost':"{:,}".format(self.prices[SELL_MIN]), 'quality':self.quality}
        return json.dumps(dataDict).strip("\n")

    def extractTierNumber(self):
        tierDict={"Beginner":1, "Novice":2, "Journeyman":3, "Adept":4, "Expert":5, "Master":6, "Grandmaster":7, "Elder":8}
        for itemTierPair in tierDict.items():
            if itemTierPair[0] in self.name:
                return itemTierPair[1]
        return 0


    def evalProfit(self):
        if self.prices[SELL_MIN] < self.prices[BUY_MAX] and self.prices[SELL_MIN] != 0 and self.prices[BUY_MAX] != 0:
            return True
        else:
            return False

    def setDate(self, dateTypeString, valueIn):
        strings = ["buy_min", "buy_max", "sell_min", "sell_max"]
        if dateTypeString in strings:
            if dateTypeString == "buy_min":
                self.dates[BUY_MIN] = valueIn
            if dateTypeString == "buy_max":
                self.dates[BUY_MAX] = valueIn
            if dateTypeString == "sell_min":
                self.dates[SELL_MIN] = valueIn
            if dateTypeString == "sell_max":
                self.dates[SELL_MAX] = valueIn


    def getDate(self, dateTypeString):
        varMap = {'buy_min':self.dates[BUY_MIN], 'buy_max':self.dates[BUY_MAX], 'sell_min':self.dates[SELL_MIN], 'sell_max':self.dates[SELL_MAX]}
        if dateTypeString in varMap:
            return varMap[dateTypeString]
        else:
            return "N/A"

    def isRecent(self):
        #returns True if sMin bMax are both within RECENT_HOURS of current time
        now = datetime.now()
        recentCutoffSell = datetime.now() - timedelta(hours = self.sell_time[0], minutes = self.sell_time[1])
        recentCutoffBuy = datetime.now() - timedelta(hours = self.buy_time[0], minutes = self.buy_time[1])

        sMin = datetime.strptime('2020/' + self.dates[SELL_MIN], '%Y/%m/%d %H:%M:%S')
        bMax = datetime.strptime('2020/' + self.dates[BUY_MAX], '%Y/%m/%d %H:%M:%S')

        if recentCutoffSell < sMin < now and recentCutoffBuy < bMax < now:
            return True
        else:
            #print("{}, {}, {}".format(recentCutoff, sMin, now))
            return False

    def filterItem(self):
        if FILTER_BYPASS:
            return True

        if self.isRecent():
            if self.sell_filter_lock:
                if self.sell_filter_lock == self.min_sell_location:
                    return True
            else:
                return True
        else:
            return False


class itemGroup(object):
    def __init__(self, itemIDListIn, argsFromGui):
        self.itemIDList = itemIDListIn
        self.locations = LOCATIONS
        self.itemIdString = ""
        self.argsFromGui = argsFromGui
        self.args = argsFromGui[3]
        self.tax = argsFromGui[3]["tax"]
        self.itemDict = self.generateItems()
        self.URL = self.createAPIAddress()
        self.valuableItems = {}
        self.profit = 0


    def createAPIAddress(self):
        baseURL = self.args["url"]
        for ID in self.itemIDList:
            baseURL += "{},".format(ID.strip('\n'))

        baseURL += "?locations="
        for location in self.locations:
            baseURL += "{},".format(location)

        baseURL += "&qualities=" #all qualities
        return baseURL

    def generateItems(self):
        itemDictOut = {}
        MIN_QUALITY = 1
        MAX_QUALITY = 5
        for itemID in self.itemIDList:
            for quality in range(MIN_QUALITY, MAX_QUALITY+1):
                newItem = Item(itemID, quality, self.args)
                itemDictOut[(itemID, quality)] = newItem
        return itemDictOut

    def fetchPrices(self):
        response = requests.get(self.URL)
        self.argsFromGui[2] += "Response Code: {}\n".format(response.status_code)
        requestJSON = response.json()

        for line in requestJSON:
            lineID = line["item_id"]
            lineQuality = line["quality"]
            if lineQuality not in [1,2,3,4,5]:
                #print("WARN: {} API got invalid quantity data; ignoring.")
                continue

            lineItem = self.itemDict[(lineID, lineQuality)]
            if lineItem.prices[SELL_MIN] == 0 or line["sell_price_min"] < lineItem.prices[SELL_MIN]:
                if lineItem.sell_lock:
                    if lineItem.sell_lock == line["city"]:
                        lineItem.prices[SELL_MIN] = line["sell_price_min"]
                        lineItem.min_sell_location = line["city"]
                        lineItem.dates[SELL_MIN] = line["sell_price_min_date"]

                else:
                    lineItem.prices[SELL_MIN] = line["sell_price_min"]
                    lineItem.min_sell_location = line["city"]
                    lineItem.dates[SELL_MIN] = line["sell_price_min_date"]

            if line["sell_price_max"] > lineItem.prices[SELL_MAX]:
                lineItem.prices[SELL_MAX] = line["sell_price_max"]
                lineItem.dates[SELL_MAX] = line["sell_price_max_date"]

            if lineItem.prices[BUY_MIN] == 0 or line["buy_price_min"] < lineItem.prices[BUY_MIN]:
                lineItem.prices[BUY_MIN] = line["buy_price_min"]
                lineItem.dates[BUY_MIN] = line["buy_price_min_date"]

            if line["buy_price_max"] > lineItem.prices[BUY_MAX]:
                if lineItem.buy_lock:
                    if lineItem.buy_lock == line["city"]:
                        lineItem.prices[BUY_MAX] = line["buy_price_max"]
                        lineItem.max_buy_location = line["city"]
                        lineItem.dates[BUY_MAX] = line["buy_price_max_date"]
                else:
                    lineItem.prices[BUY_MAX] = line["buy_price_max"]
                    lineItem.max_buy_location = line["city"]
                    lineItem.dates[BUY_MAX] = line["buy_price_max_date"]


    def evalProfit(self):
        itemList = list(self.itemDict.values())

        for item in itemList:
            dates = ["buy_min", "buy_max", "sell_min", "sell_max"]
            for dateString in dates:
                date = item.getDate(dateString)
                dateObject = datetime.strptime(date + ' +0000', '%Y-%m-%dT%H:%M:%S %z')
                if dateObject.year > 2017:
                    item.setDate(dateString, dateObject.astimezone().strftime('%m/%d %H:%M:%S'))
                else:
                    item.setDate(dateString, dateObject.strftime('%m/%d %H:%M:%S'))

        for item in itemList:
            item.profit = (item.prices[BUY_MAX] // (1+self.tax))-item.prices[SELL_MIN]
            item.bmSellProfit = (item.bmPrice // (1+self.tax+Item.auctionFee))-item.prices[SELL_MIN]
            if item.filterItem():
                if item.min_sell_location not in self.valuableItems:
                    self.valuableItems[item.min_sell_location] = []
                self.valuableItems[item.min_sell_location].append(item)


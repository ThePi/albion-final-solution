
# -*- coding: utf-8 -*-

from remi.gui import *
from remi import start, App
import albion_api
import threading
import time
import ssl
import random



class untitled(App):
    def __init__(self, *args, **kwargs):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        if not 'editing_mode' in kwargs.keys():
            super(untitled, self).__init__(*args, static_file_path={'my_res':'./res/'})

        #in progress, done, result/Debug string, args (dict), item string dict
        self.runningTradesDone = [False, False, "", {}, {}]

    def idle(self):
        if self.runningTradesDone[0]:
            self.cnt_settings.get_child("cnt_results").get_child("ti_resulttext").set_text(self.runningTradesDone[2])

        if self.runningTradesDone[1]:

            self.cnt_settings.get_child("lb_status").set_text("Ready")
            self.cnt_settings.get_child("cnt_results").get_child("ti_resulttext").set_text(self.runningTradesDone[2])


            cnt_result = self.cnt_settings.get_child("cnt_results")
            tb_results = cnt_result.get_child("tb_results")
            for cityResultPair in list(self.runningTradesDone[4].items()):
                box = TextInput()
                title = cityResultPair[0]
                box.css_height = "90%"
                box.css_width = "97%"
                box.css_position = "absolute"
                box.css_margin = "10px"
                box.set_text(cityResultPair[1])

                tb_results.append(box, title)

            #reset for next
            self.runningTradesDone = [False, False, "", {}, {}]

        #idle function called every update cycle
        pass

    def main(self):
        ui = untitled.construct_ui(self)


        self.cnt_settings.get_child("lb_status").set_text("Ready")

        self.cnt_settings.get_child("bt_default").onclick.do(self.set_defaults)
        self.cnt_settings.get_child("bt_clear").onclick.do(self.clear_fields)
        self.cnt_settings.get_child("bt_run").onclick.do(self.run_trades)
        self.cnt_settings.get_child("bt_addtab").onclick.do(self.add_tab)
        return ui

    def set_defaults(self, widget):
        print("DEBUG: setting defaults")
        sell_lock = self.cnt_settings.get_child("ti_selllock")
        sell_filter_lock = self.cnt_settings.get_child("ti_sellfilterlock")
        buy_lock = self.cnt_settings.get_child("ti_buylock")
        tax = self.cnt_settings.get_child("ti_taxpercent")
        sell_time = self.cnt_settings.get_child("ti_selltime")
        buy_time = self.cnt_settings.get_child("ti_buytime")
        url = self.cnt_settings.get_child("ti_baseurl")
        sortmethod = self.cnt_settings.get_child("ti_sortmethod")

        sell_lock.set_text("")
        sell_filter_lock.set_text("Martlock")
        buy_lock.set_text("Black Market")
        sortmethod.set_text("profit")

        tax.set_text(".03")
        sell_time.set_text("1.5")
        buy_time.set_text(".20")

        url.set_text("http://localhost:5000/api/v2/stats/Prices/")

    def clear_fields(self, widget):
        cnt_results = self.cnt_settings.get_child("cnt_results")
        tb_results = cnt_results.get_child("tb_results")
        sell_lock = self.cnt_settings.get_child("ti_selllock")
        sell_filter_lock = self.cnt_settings.get_child("ti_sellfilterlock")
        buy_lock = self.cnt_settings.get_child("ti_buylock")
        tax = self.cnt_settings.get_child("ti_taxpercent")
        sell_time = self.cnt_settings.get_child("ti_selltime")
        buy_time = self.cnt_settings.get_child("ti_buytime")
        url = self.cnt_settings.get_child("ti_baseurl")
        status = self.cnt_settings.get_child("lb_status")
        result_text = cnt_results.get_child("ti_resulttext")
        sortmethod = self.cnt_settings.get_child("ti_sortmethod")

        sell_lock.set_text("")
        sell_filter_lock.set_text("")
        buy_lock.set_text("")

        tax.set_text("")
        sell_time.set_text("")
        buy_time.set_text("")

        url.set_text("")

        status.set_text("Ready")
        result_text.set_text("")

        sortmethod.set_text("")
        
        
        cnt_results.remove_child(tb_results)
        
        newBox = TabBox()
        newBox.attr_editor_newclass = False
        newBox.css_background_color = "rgb(255,255,255)"
        newBox.css_height = "855.0px"
        newBox.css_left = "15.0px"
        newBox.css_margin = "0px"
        newBox.css_position = "absolute"
        newBox.css_top = "60.0px"
        newBox.css_width = "1220.0px"
        newBox.variable_name = "tb_results"
        cnt_results.append(newBox,'tb_results')
        

    def run_trades(self, widget):
        cnt_result = self.cnt_settings.get_child("cnt_results")

        sell_lock = self.cnt_settings.get_child("ti_selllock")
        sell_filter_lock = self.cnt_settings.get_child("ti_sellfilterlock")
        buy_lock = self.cnt_settings.get_child("ti_buylock")
        tax = self.cnt_settings.get_child("ti_taxpercent")
        sell_time = self.cnt_settings.get_child("ti_selltime")
        buy_time = self.cnt_settings.get_child("ti_buytime")
        url = self.cnt_settings.get_child("ti_baseurl")
        sortmethod = self.cnt_settings.get_child("ti_sortmethod")

        print("Running trades...")
        status = self.cnt_settings.get_child("lb_status")

        status.set_text("Running...")
        self.runningTradesDone[3]["sell_lock"] = sell_lock.text
        self.runningTradesDone[3]["buy_lock"] = buy_lock.text
        self.runningTradesDone[3]["sell_filter_lock"] = sell_filter_lock.text
        self.runningTradesDone[3]["tax"] = float(tax.text)
        self.runningTradesDone[3]["url"] = url.text
        self.runningTradesDone[3]["sortmethod"] = sortmethod.text
        self.runningTradesDone[3]["sell_time"] = sell_time.text
        self.runningTradesDone[3]["buy_time"] = buy_time.text

        t = threading.Thread(target=albion_api.generateTrades, args=(self.runningTradesDone,))
        t.start()



    def add_tab(self, widget):
        cnt_result = self.cnt_settings.get_child("cnt_results")
        tb_results = cnt_result.get_child("tb_results")
        
        box = TextInput()
        title = "r"+str(random.randint(1,10))
        box.css_height = "90%"
        box.css_width = "97%"
        box.css_position = "absolute"
        box.css_margin = "10px"
        
        tb_results.append(box, title)


    @staticmethod
    def construct_ui(self):
        # DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        cnt_settings = Container()
        cnt_settings.attr_editor_newclass = False
        cnt_settings.css_height = "930.0px"
        cnt_settings.css_left = "0.0px"
        cnt_settings.css_margin = "0px"
        cnt_settings.css_position = "absolute"
        cnt_settings.css_top = "0.0px"
        cnt_settings.css_width = "555.0px"
        cnt_settings.variable_name = "cnt_settings"
        cnt_results = Container()
        cnt_results.attr_editor_newclass = False
        cnt_results.css_height = "930.0px"
        cnt_results.css_left = "570.0px"
        cnt_results.css_margin = "0px"
        cnt_results.css_position = "absolute"
        cnt_results.css_top = "0.0px"
        cnt_results.css_width = "765.0px"
        cnt_results.variable_name = "cnt_results"
        lb_results = Label()
        lb_results.attr_editor_newclass = False
        lb_results.css_height = "30px"
        lb_results.css_left = "20px"
        lb_results.css_margin = "0px"
        lb_results.css_position = "absolute"
        lb_results.css_top = "20px"
        lb_results.css_width = "100px"
        lb_results.text = "Results:"
        lb_results.variable_name = "lb_results"
        cnt_results.append(lb_results, 'lb_results')
        tb_results = TabBox()
        tb_results.attr_editor_newclass = False
        tb_results.css_background_color = "rgb(255,255,255)"
        tb_results.css_height = "855.0px"
        tb_results.css_left = "15.0px"
        tb_results.css_margin = "0px"
        tb_results.css_position = "absolute"
        tb_results.css_top = "60.0px"
        tb_results.css_width = "1220.0px"
        tb_results.variable_name = "tb_results"
        cnt_results.append(tb_results, 'tb_results')
        ti_resulttext = TextInput()
        ti_resulttext.attr_editor_newclass = False
        ti_resulttext.css_height = "465.0px"
        ti_resulttext.css_left = "-330.0px"
        ti_resulttext.css_margin = "0px"
        ti_resulttext.css_position = "absolute"
        ti_resulttext.css_top = "195.0px"
        ti_resulttext.css_width = "300.0px"
        ti_resulttext.text = ""
        ti_resulttext.variable_name = "ti_resulttext"
        cnt_results.append(ti_resulttext, 'ti_resulttext')
        cnt_settings.append(cnt_results, 'cnt_results')
        bt_run = Button()
        bt_run.attr_editor_newclass = False
        bt_run.css_height = "30px"
        bt_run.css_left = "15.0px"
        bt_run.css_margin = "0px"
        bt_run.css_position = "absolute"
        bt_run.css_top = "30.0px"
        bt_run.css_width = "100px"
        bt_run.text = "Run"
        bt_run.variable_name = "bt_run"
        cnt_settings.append(bt_run, 'bt_run')
        lb_statusinfo = Label()
        lb_statusinfo.attr_editor_newclass = False
        lb_statusinfo.css_height = "30px"
        lb_statusinfo.css_left = "225.0px"
        lb_statusinfo.css_margin = "0px"
        lb_statusinfo.css_position = "absolute"
        lb_statusinfo.css_top = "15.0px"
        lb_statusinfo.css_width = "100px"
        lb_statusinfo.text = "STATUS:"
        lb_statusinfo.variable_name = "lb_statusinfo"
        cnt_settings.append(lb_statusinfo, 'lb_statusinfo')
        bt_clear = Button()
        bt_clear.attr_editor_newclass = False
        bt_clear.css_height = "30px"
        bt_clear.css_left = "435.0px"
        bt_clear.css_margin = "0px"
        bt_clear.css_position = "absolute"
        bt_clear.css_top = "30.0px"
        bt_clear.css_width = "100px"
        bt_clear.text = "Clear"
        bt_clear.variable_name = "bt_clear"
        cnt_settings.append(bt_clear, 'bt_clear')
        ti_selllock = TextInput()
        ti_selllock.attr_editor_newclass = False
        ti_selllock.css_height = "30.0px"
        ti_selllock.css_left = "15.0px"
        ti_selllock.css_margin = "0px"
        ti_selllock.css_position = "absolute"
        ti_selllock.css_top = "135.0px"
        ti_selllock.css_width = "150.0px"
        ti_selllock.text = ""
        ti_selllock.variable_name = "ti_selllock"
        cnt_settings.append(ti_selllock, 'ti_selllock')
        ti_sellfilterlock = TextInput()
        ti_sellfilterlock.attr_editor_newclass = False
        ti_sellfilterlock.css_height = "30.0px"
        ti_sellfilterlock.css_left = "15.0px"
        ti_sellfilterlock.css_margin = "0px"
        ti_sellfilterlock.css_position = "absolute"
        ti_sellfilterlock.css_top = "210.0px"
        ti_sellfilterlock.css_width = "150.0px"
        ti_sellfilterlock.text = ""
        ti_sellfilterlock.variable_name = "ti_sellfilterlock"
        cnt_settings.append(ti_sellfilterlock, 'ti_sellfilterlock')
        lb_selllock = Label()
        lb_selllock.attr_editor_newclass = False
        lb_selllock.css_height = "30px"
        lb_selllock.css_left = "15.0px"
        lb_selllock.css_margin = "0px"
        lb_selllock.css_position = "absolute"
        lb_selllock.css_top = "105.0px"
        lb_selllock.css_width = "100px"
        lb_selllock.text = "Sell Lock"
        lb_selllock.variable_name = "lb_selllock"
        cnt_settings.append(lb_selllock, 'lb_selllock')
        ti_buylock = TextInput()
        ti_buylock.attr_editor_newclass = False
        ti_buylock.css_height = "30.0px"
        ti_buylock.css_left = "15.0px"
        ti_buylock.css_margin = "0px"
        ti_buylock.css_position = "absolute"
        ti_buylock.css_top = "285.0px"
        ti_buylock.css_width = "150.0px"
        ti_buylock.text = ""
        ti_buylock.variable_name = "ti_buylock"
        cnt_settings.append(ti_buylock, 'ti_buylock')
        
        lb_sortmethod = Label()
        lb_sortmethod.attr_editor_newclass = False
        lb_sortmethod.css_height = "30.0px"
        lb_sortmethod.css_left = "15.0px"
        lb_sortmethod.css_margin = "0px"
        lb_sortmethod.css_position = "absolute"
        lb_sortmethod.css_top = "315.0px"
        lb_sortmethod.css_width = "250.0px"
        lb_sortmethod.text = "Sort Method ('profit', 'bmProfit')"
        lb_sortmethod.variable_name = "lb_sortmethod"
        cnt_settings.append(lb_sortmethod, 'lb_sortmethod')
        ti_sortmethod = TextInput()
        ti_sortmethod.attr_editor_newclass = False
        ti_sortmethod.css_height = "30.0px"
        ti_sortmethod.css_left = "15.0px"
        ti_sortmethod.css_margin = "0px"
        ti_sortmethod.css_position = "absolute"
        ti_sortmethod.css_top = "345.0px"
        ti_sortmethod.css_width = "150.0px"
        ti_sortmethod.text = ""
        ti_sortmethod.variable_name = "ti_sortmethod"
        cnt_settings.append(ti_sortmethod, 'ti_sortmethod')
        
        
        
        lb_sellfilterlock = Label()
        lb_sellfilterlock.attr_editor_newclass = False
        lb_sellfilterlock.css_height = "30px"
        lb_sellfilterlock.css_left = "15.0px"
        lb_sellfilterlock.css_margin = "0px"
        lb_sellfilterlock.css_position = "absolute"
        lb_sellfilterlock.css_top = "180.0px"
        lb_sellfilterlock.css_width = "100px"
        lb_sellfilterlock.text = "Sell Filter Lock"
        lb_sellfilterlock.variable_name = "lb_sellfilterlock"
        cnt_settings.append(lb_sellfilterlock, 'lb_sellfilterlock')
        lb_buylock = Label()
        lb_buylock.attr_editor_newclass = False
        lb_buylock.css_height = "30px"
        lb_buylock.css_left = "15.0px"
        lb_buylock.css_margin = "0px"
        lb_buylock.css_position = "absolute"
        lb_buylock.css_top = "255.0px"
        lb_buylock.css_width = "100px"
        lb_buylock.text = "Buy Lock"
        lb_buylock.variable_name = "lb_buylock"
        cnt_settings.append(lb_buylock, 'lb_buylock')
        lb_status = Label()
        lb_status.attr_editor_newclass = False
        lb_status.css_height = "30px"
        lb_status.css_left = "225.0px"
        lb_status.css_margin = "0px"
        lb_status.css_position = "absolute"
        lb_status.css_top = "45.0px"
        lb_status.css_width = "100px"
        lb_status.text = "Status"
        lb_status.variable_name = "lb_status"
        cnt_settings.append(lb_status, 'lb_status')
        lb_baseurl = Label()
        lb_baseurl.attr_editor_newclass = False
        lb_baseurl.css_height = "30px"
        lb_baseurl.css_left = "15.0px"
        lb_baseurl.css_margin = "0px"
        lb_baseurl.css_position = "absolute"
        lb_baseurl.css_top = "645.0px"
        lb_baseurl.css_width = "100px"
        lb_baseurl.text = "Base URL:"
        lb_baseurl.variable_name = "lb_baseurl"
        cnt_settings.append(lb_baseurl, 'lb_baseurl')
        ti_baseurl = TextInput()
        ti_baseurl.attr_editor_newclass = False
        ti_baseurl.css_height = "45.0px"
        ti_baseurl.css_left = "15.0px"
        ti_baseurl.css_margin = "0px"
        ti_baseurl.css_position = "absolute"
        ti_baseurl.css_top = "675.0px"
        ti_baseurl.css_width = "510.0px"
        ti_baseurl.text = ""
        ti_baseurl.variable_name = "ti_baseurl"
        cnt_settings.append(ti_baseurl, 'ti_baseurl')
        lb_selltime = Label()
        lb_selltime.attr_editor_newclass = False
        lb_selltime.css_height = "30.0px"
        lb_selltime.css_left = "15.0px"
        lb_selltime.css_margin = "0px"
        lb_selltime.css_position = "absolute"
        lb_selltime.css_top = "480.0px"
        lb_selltime.css_width = "135.0px"
        lb_selltime.text = "Sell Time (0-24hr)"
        lb_selltime.variable_name = "lb_selltime"
        cnt_settings.append(lb_selltime, 'lb_selltime')
        lb_buytime = Label()
        lb_buytime.attr_editor_newclass = False
        lb_buytime.css_height = "30.0px"
        lb_buytime.css_left = "15.0px"
        lb_buytime.css_margin = "0px"
        lb_buytime.css_position = "absolute"
        lb_buytime.css_top = "555.0px"
        lb_buytime.css_width = "135.0px"
        lb_buytime.text = "Buy Time (0-24hr)"
        lb_buytime.variable_name = "lb_buytime"
        cnt_settings.append(lb_buytime, 'lb_buytime')
        lb_taxinfo = Label()
        lb_taxinfo.attr_editor_newclass = False
        lb_taxinfo.css_height = "15.0px"
        lb_taxinfo.css_left = "15.0px"
        lb_taxinfo.css_margin = "0px"
        lb_taxinfo.css_position = "absolute"
        lb_taxinfo.css_top = "375.0px"
        lb_taxinfo.css_width = "210.0px"
        lb_taxinfo.text = "Tax Percent (.03 or .06 usually)"
        lb_taxinfo.variable_name = "lb_taxinfo"
        cnt_settings.append(lb_taxinfo, 'lb_taxinfo')
        ti_taxpercent = TextInput()
        ti_taxpercent.attr_editor_newclass = False
        ti_taxpercent.css_height = "30px"
        ti_taxpercent.css_left = "15.0px"
        ti_taxpercent.css_margin = "0px"
        ti_taxpercent.css_position = "absolute"
        ti_taxpercent.css_top = "405.0px"
        ti_taxpercent.css_width = "100px"
        ti_taxpercent.text = ""
        ti_taxpercent.variable_name = "ti_taxpercent"
        cnt_settings.append(ti_taxpercent, 'ti_taxpercent')
        bt_default = Button()
        bt_default.attr_editor_newclass = False
        bt_default.css_height = "30px"
        bt_default.css_left = "435.0px"
        bt_default.css_margin = "0px"
        bt_default.css_position = "absolute"
        bt_default.css_top = "75.0px"
        bt_default.css_width = "100px"
        bt_default.text = "Defaults"
        bt_default.variable_name = "bt_default"
        cnt_settings.append(bt_default, 'bt_default')
        label0 = Label()
        label0.attr_editor_newclass = False
        label0.css_height = "30.0px"
        label0.css_left = "15.0px"
        label0.css_margin = "0px"
        label0.css_position = "absolute"
        label0.css_top = "780.0px"
        label0.css_width = "360.0px"
        label0.text = "http://www.albion-online-data.com/api/v2/stats/prices/, http://localhost:5000/api/v2/stats/Prices/"
        label0.variable_name = "label0"
        cnt_settings.append(label0, 'label0')
        label1 = Label()
        label1.attr_editor_newclass = False
        label1.css_height = "0.0px"
        label1.css_left = "15.0px"
        label1.css_margin = "0px"
        label1.css_position = "absolute"
        label1.css_top = "750.0px"
        label1.css_width = "165.0px"
        label1.text = "Common API URLs:"
        label1.variable_name = "label1"
        cnt_settings.append(label1, 'label1')
        bt_addtab = Button()
        bt_addtab.attr_editor_newclass = False
        bt_addtab.css_height = "30px"
        bt_addtab.css_left = "435.0px"
        bt_addtab.css_margin = "0px"
        bt_addtab.css_position = "absolute"
        bt_addtab.css_top = "120.0px"
        bt_addtab.css_width = "100px"
        bt_addtab.text = "Add Tab"
        bt_addtab.variable_name = "bt_addtab"
        cnt_settings.append(bt_addtab, 'bt_addtab')
        lb_runlog = Label()
        lb_runlog.attr_editor_newclass = False
        lb_runlog.css_height = "30px"
        lb_runlog.css_left = "240.0px"
        lb_runlog.css_margin = "0px"
        lb_runlog.css_position = "absolute"
        lb_runlog.css_top = "150.0px"
        lb_runlog.css_width = "100px"
        lb_runlog.text = "Run Log"
        lb_runlog.variable_name = "lb_runlog"
        cnt_settings.append(lb_runlog, 'lb_runlog')
        ti_selltime = TextInput()
        ti_selltime.attr_editor_newclass = False
        ti_selltime.css_height = "30px"
        ti_selltime.css_left = "15.0px"
        ti_selltime.css_margin = "0px"
        ti_selltime.css_position = "absolute"
        ti_selltime.css_top = "510.0px"
        ti_selltime.css_width = "100px"
        ti_selltime.text = ""
        ti_selltime.variable_name = "ti_selltime"
        cnt_settings.append(ti_selltime, 'ti_selltime')
        ti_buytime = TextInput()
        ti_buytime.attr_editor_newclass = False
        ti_buytime.css_height = "30px"
        ti_buytime.css_left = "15.0px"
        ti_buytime.css_margin = "0px"
        ti_buytime.css_position = "absolute"
        ti_buytime.css_top = "585.0px"
        ti_buytime.css_width = "100px"
        ti_buytime.text = ""
        ti_buytime.variable_name = "ti_buytime"
        cnt_settings.append(ti_buytime, 'ti_buytime')

        self.cnt_settings = cnt_settings
        return self.cnt_settings


#Configuration
configuration = {'config_project_name': 'WISCO WORLDWIDE', 'config_address': '0.0.0.0', 'config_port': 8081, 'config_multiple_instance': True, 'config_enable_file_cache': True, 'config_start_browser': True, 'config_resourcepath': './res/', 'update_interval': 0.1}

if __name__ == "__main__":
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    start(untitled, address=configuration['config_address'], port=configuration['config_port'], 
                        multiple_instance=configuration['config_multiple_instance'], 
                        enable_file_cache=configuration['config_enable_file_cache'],
                        start_browser=configuration['config_start_browser'],
                        update_interval=configuration['update_interval'],
                        title="WISCO WORLDWIDE INC.",
                        certfile="/etc/letsencrypt/live/tmubl.com/fullchain.pem",
                        keyfile="/etc/letsencrypt/live/tmubl.com/privkey.pem",
                        ssl_version = ssl.PROTOCOL_TLSv1_2)

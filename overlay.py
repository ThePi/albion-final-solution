# IMPORTS
from screeninfo import get_monitors
import pytesseract
import time
import tkinter
from PIL import Image
from PIL import ImageGrab
import requests
import json
import win32api, win32con, pywintypes
import logging
from tkinter import font


# CONFIGURATION
RESOLUTION = "", ""
VERSION = "0.4.3"
TEXTBOX_COLOR = '#%02x%02x%02x' % (130, 189, 172)
logging.basicConfig(level = logging.INFO)



# CLASSES
class CriticalError:
    def __init__(self, text):
        logging.error(text)
        exit(1)


class GetResolution:
    def __init__(self):
        logging.info("Getting screen resolution...")
        found = False
        for monitor in get_monitors():
            print(monitor)
                #if str(monitor).split('+')[1] == '0':
                #resolution_string = str(monitor).split('+')[0].split('(')[1]
            self.width = 2560
            self.height = 1440
            self.k = int(self.height) / 1080
            found = True
        if found == False:
            CriticalError('Can\'t find display resolution!')

    @property
    def resolution(self):
        logging.info("Resolution is {}x{}".format(self.width, self.height))
        return int(self.width), int(self.height), float(self.k)

TEXT_COORDS = {"title":(860, 315), "tier":(1280, 315), "enchant":(1465, 315), "quality":(1645, 315), "cost":(1370,250), "itemnumber":(1600,250)}
class InitOverlay():
    def __init__(self, textIn):
        self.root = tkinter.Tk()
        self.root.overrideredirect(True)
        self.root.wm_attributes("-transparentcolor", "white")
        self.root.wm_attributes("-topmost", True)
        self.root.wm_attributes("-disabled", False)
        self.canvas = tkinter.Canvas(self.root, width = RESOLUTION[0], height = RESOLUTION[1], bg = "white", highlightthickness = 0)
        self.smallFont = font.Font(family="Courier New", size=16, weight="bold")
        self.largeFont = font.Font(family="Courier New", size=24, weight="bold")
        self.veryLargeFont = font.Font(family="Courier New", size=36, weight="bold")
        self.items = []
        self.text = textIn
        self.generateItemsFromText()

        # self.items.append({"title":"Royal Armor", "tier":"8", "enchant":"1", "quality":"1", "cost":"452,500"})
        # self.items.append({"title": "Hellion Hood", "tier": "6", "enchant": "2", "quality": "4", "cost":"125,000"})
        # self.items.append({"title": "Soldier Helmet", "tier": "7", "enchant": "3", "quality": "2", "cost":"1,125,000"})

        self.textPosition = 0
        self.canvas.pack()
        self.launch()

    def generateItemsFromText(self):
        itemsList = self.text.split(';')
        for item in itemsList:
            try:
                i = json.loads(item)
                print("<{}>".format(i))
                self.items.append(i)
            except:
                print("Unable to serialize JSON string '{}'".format(item))


    def launch(self):
        self.redraw_canvas()
        self.root.mainloop()

    def redraw_canvas(self):
        self.canvas.delete("all")
        #logging.info("Drawing test text...")
        self.drawTextBox('title', self.smallFont)
        self.drawTextBox('tier', self.largeFont)
        self.drawTextBox('enchant', self.largeFont)
        self.drawTextBox('quality', self.largeFont)
        self.drawTextBox('cost', self.veryLargeFont)
        self.drawTextBox('itemnumber', self.largeFont)


        self.root.bind('<Enter>', lambda event: self.root.focus_set())
        self.root.bind('<Left>', self.leftKey)
        self.root.bind('<Right>', self.rightKey)
        self.root.bind('<space>', self.rightKey)

        #logging.info("Sleeping for {} seconds..".format(UPDATE_INTERVAL))
        self.canvas.after(100, self.redraw_canvas)

    def drawTextBox(self, field, fontIn):
        if field is 'itemnumber':
            textIn = "{}/{}".format(self.textPosition, len(self.items))
        else:
            textIn = self.items[self.textPosition][field]
        i = self.canvas.create_text(TEXT_COORDS[field], text=textIn, font=fontIn)
        r = self.canvas.create_rectangle(self.canvas.bbox(i), fill=TEXTBOX_COLOR)
        self.canvas.tag_lower(r, i)

    def leftKey(self, event):
        logging.info("left")
        if self.textPosition is 0:
            self.textPosition = len(self.items) - 1
        else:
            self.textPosition -= 1

    def rightKey(self, event):
        logging.info("right")
        if self.textPosition is len(self.items) - 1:
            self.textPosition = 0
        else:
            self.textPosition += 1


class Information():
    def __init__(self):
        logging.info("WISCO WORLDWIDE")
        logging.info("Forked from ph5x5's map tool")
        logging.info("Version: {}".format(VERSION))



# MAIN
def main():
    global RESOLUTION
    textIn = input("Enter in text from web app:\n")
    print("---\n{}".format(textIn))
    Information()
    RESOLUTION = GetResolution().resolution
    InitOverlay(textIn)



# BODY
if __name__ == "__main__":
    main()

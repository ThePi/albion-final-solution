SELL_MIN = 0
SELL_MAX = 1
BUY_MIN = 2
BUY_MAX = 3
ITEMS_PER_REQUEST = 200
MIN_Q = 1
MAX_Q = 5
DEFAULT_RESOURCE_QUALITY = 1

"""
https://www.lucidchart.com/documents/edit/6c2263fa-5434-4fe6-93b3-8ffded553cbe/0_0
https://wiki.albiononline.com/wiki/Crafting_Focus
https://wiki.albiononline.com/wiki/Crafting
https://gameinfo.albiononline.com/api/gameinfo/items/T5_BAG/data
https://forum.albiononline.com/index.php/Thread/67684-Crafting-quality-chance/

"""
class TradeControl(object):
    argsIn = []
    items = {}
    itemsQual = {}
    idToName = {}
    nameAliases = {}
    allItemIDs = []
    numToID = {}
    def __init__(self, argsIn_):
        TradeControl.argsIn = argsIn_
        self.sortMethod = argsIn_[3]["sortmethod"]
        # Min, Max, useQuality (True for armor, false for ore, etc.)
        self.itemsToCheck = [[2150, 3610, True], [4301, 6119, True], [618, 825, False]]  # melee, armor, resources
        #Set static vars now
        #Load IDs
        TradeControl.LoadItemIDs()

    def CreateItems(self):
        #TODONE Generate items based on itemsToCheck and add to items{}
        for itemRange in self.itemsToCheck:
            for itemNum in range(itemRange[0], itemRange[1] + 1):
                ID = TradeControl.GetIdByNum(itemNum)
                itemName = TradeControl.GetNameById(ID)
                # If using quality, make 5, add them all, and then make a list and add a quality group
                if itemRange[2]:
                    itemDictToQualGroup = {}
                    for quality in range(MIN_Q, MAX_Q + 1):
                        newItem = Item(ID, itemName, quality)
                        TradeControl.items[(ID, quality)] = newItem
                        itemDictToQualGroup[quality] = newItem

                    TradeControl.itemsQual[ID] = ItemQualClass(itemDictToQualGroup)
                else:
                    # Else, it's a resource, we only need make the 1 and add it with quality '1'
                    newItem = Item(ID, itemName, DEFAULT_RESOURCE_QUALITY)
                    TradeControl.items[(ID, DEFAULT_RESOURCE_QUALITY)] = newItem

        TradeControl.AppendTerminal("Finished setting up items\n")
        pass

    @staticmethod
    def AppendTerminal(line_):
        TradeControl.argsIn[2] += line_

    @staticmethod
    def ClearTerminal():
        TradeControl.argsIn[2] = ""

    @staticmethod
    def GetItemNameQual(name, quality):
        if (name, quality) in TradeControl.items:
            return TradeControl.items[(name, quality)]

        if name in TradeControl.nameAliases and (TradeControl.nameAliases[name], quality) in TradeControl.items:
            return TradeControl.items[TradeControl.nameAliases[name]]

        print("WARN: GetItemNameQual returned 'None' : ({},{})".format(name, quality))
        return None

    @staticmethod
    def GetItemQualByID(ID):
        if ID in TradeControl.itemsQual:
            return TradeControl.itemsQual[ID]

        if ID in TradeControl.nameAliases and TradeControl.nameAliases[ID] in TradeControl.itemsQual:
            return TradeControl.itemsQual[TradeControl.nameAliases[ID]]

        print("WARN: GetItemQualByName returned 'None' : ({})".format(ID))
        return None

    @staticmethod
    def LoadItemIDs():
        itemsFile = open("items.txt", "r")
        itemsText = itemsFile.readlines()
        for lineNum in range(len(itemsText)):
            parts = itemsText[lineNum].split(":")
            itemID = parts[1].replace(" ", "")
            if len(parts) < 3:
                prettyText = "PLACEHOLDER"
            else:
                prettyText = parts[2].strip()
            TradeControl.idToName[itemID] = prettyText
            TradeControl.allItemIDs.append(itemID)
            secondaryItemId = itemID.replace("@", "_LEVEL")

            # Add the num --> ID
            TradeControl.numToID[parts[0]] = itemID
            # Also add the _LEVEL syntax that the albion item info API uses
            TradeControl.nameAliases[itemID] = secondaryItemId
            TradeControl.nameAliases[secondaryItemId] = itemID
        itemsFile.close()

    @staticmethod
    def GetNameById(id_):
        if id_ in TradeControl.idToName:
            return TradeControl.idToName[id_]
        else:
            print("WARN: GetNameById returned 'None' : {}".format(id_))
            return None

    @staticmethod
    def GetIdByNum(num_):
        if num_ in TradeControl.numToID:
            return TradeControl.numToID[num_]
        else:
            print("WARN: GetIDByNum returned 'None' : {}".format(num_))
            return None

    def FetchItemPrices(self, itemsToFetch_):
        #TODO: Fetch item prices from market api, as per ranges given. Put prices into corresponding items
        pass

    def FetchItemHistories(self, itemsToFetch_):
        #TODOne: Fetch item historical data, for selling crafted items in city
        # Uses ItemQualClass's FetchQualHistory()
        for itemRange in itemsToFetch_:
            for itemNum in range(itemRange[0], itemRange[1] + 1):
                ID = TradeControl.GetIdByNum(itemNum)
                iQ = TradeControl.GetItemQualByID(ID)
                iQ.FetchQualHistory()

    def FetchCraftData(self, itemsToFetch_):
        #TODO: Fetch crafting data from the official albion info api
        # Note that the item IDs might be a bit off that comes from the JSON data, but it should be
        # normalized in the getters.
        pass

    def CalculateMarketTrades(self, itemsToCalc_):
        #TODO: Calculate and sort the list of valuable trades. Add each valuable trade to the right area in argsIn [4] I believe?
        # Get a list of TradeResult
        # see original outputItemsToGui(valuableItems, argsIn) for more details
        pass

    def CalculateCrafts(self, itemsToCalc_):
        #TODO: Calculate and sort the list of valid crafts. Get a list of CraftResult.
        # For now, just print them to terminal (probably separated by city, like they would be eventually in the WISCO tool.)
        #------
        #TODO: Eventually, add way to insert data into WISCO tool.
        pass

class ConfigData(object):
    stations = {
    "f":{},
    "l":{},
    "w":{},
    "t":{}
    }
    shopTypes = ["f", "t", "w", "t"]
    cities = ["Martlock", "Bridgewatch", "Lymhurst", "Fort Sterling", "Thetford", "Caerleon"]
    def __init__(self):
        """
        self.shopPrice = {
                          "Martlock": {"f": 0.16, "l": 0.15, "w": 0.12, "t": 0.13},
                          "Bridgewatch": {"f": 0.15, "l": 0.40, "w": 0.10, "t": 0.55},
                          "Lymhurst": {"f": 0.36, "l": 0.08, "w": 0.09, "t": 0.36},
                          "Fort Sterling": {"f": 0.38, "l": 0.43, "w": 0.40, "t": 0.43},
                          "Thetford": {"f": 0.54, "l": 0.52, "w": 0.52, "t": 0.54},
                          "Caerleon": {"f": 0.30, "l": 0.27, "w": 0.10, "t": 0.30}}
        """

        self.shopPrice = {
        "f":{"Martlock":0.16, "Bridgewatch":0.15,"Lymhurst":0.36,"Fort Sterling":0.38,"Thetford":0.54,"Caerleon":0.30},
        "l":{"Martlock":0.15, "Bridgewatch":0.40,"Lymhurst":0.08,"Fort Sterling":0.43,"Thetford":0.52,"Caerleon":0.27},
        "w":{"Martlock":0.12, "Bridgewatch":0.10,"Lymhurst":0.09,"Fort Sterling":0.40,"Thetford":0.52,"Caerleon":0.10},
        "t":{"Martlock":0.13, "Bridgewatch":0.55,"Lymhurst":0.36,"Fort Sterling":0.43,"Thetford":0.54,"Caerleon":0.30},
        }

        #TODO: Finish cnSetup data
        self.cnSetup = {"f":[], "l":[], "w":[], "t":[]}
        self.cnSetup["f"] = [
            ["PlateBoots", [["SHOES_PLATE_SET1"], ["SHOES_PLATE_SET2"],["SHOES_PLATE_SET3"],["SHOES_PLATE_UNDEAD","SHOES_PLATE_HELL","SHOES_PLATE_KEEPER","SHOES_PLATE_AVALON","SHOES_PLATE_ROYAL"]]],
            ["PlateArmor", [["ARMOR_PLATE_SET1"],["ARMOR_PLATE_SET2"],["ARMOR_PLATE_SET3"],["ARMOR_PLATE_UNDEAD","ARMOR_PLATE_HELL","ARMOR_PLATE_KEEPER","ARMOR_PLATE_AVALON","ARMOR_PLATE_ROYAL"]]],
            ["PlateHelm",  [["HEAD_PLATE_SET1"],["HEAD_PLATE_SET2"],["HEAD_PLATE_SET3"],["HEAD_PLATE_UNDEAD","HEAD_PLATE_HELL","HEAD_PLATE_KEEPER","HEAD_PLATE_AVALON","HEAD_PLATE_ROYAL"]]],
            ["Sword",      [["_SWORD"], ["_CLAYMORE"], ["DUALSWORD"], ["_SCIMITAR","_DUALSCIMITAR","CLEAVER_HELL"]]],
            ["Battleaxe",  [["MAIN_AXE"], ["2H_AXE"], ["'s Halberd"], ["HALBERD_MORGANA","2H_SCYTHE_HELL","DUALAXE_KEEPER"]]]
        ]

        self.cnSetup["l"] = []
        self.cnSetup["w"] = []
        self.cnSetup["t"] = []
        self.craftNodes = []
        self.SetupCraftNodes()

    def SetupCraftNodes(self):
        itemfile = open("items.txt", "r")
        itemLines = itemfile.readlines()

        # For each shop type, do the following:
        #   Create a new CraftStation for each City.
        #   For each item --> craftNode, do the following:
        #      Loop over the [1] . Fetch the list of valid IDs for each string. Create a Spec for each string.
        #      Add all Spec to a list. Combine all Spec validItems to create an aggregate. Create a CraftNode with the list of Spec and all validItems.
        #      Add the CraftNode to weaponTypes for each city
        #      Add the craftnode's valid nums to the craftstation
        #For each shop, do the following:
        for shopType in ConfigData.shopTypes:
            #Create a new CraftStation for each city
            for city in ConfigData.cities:
                fee = self.shopPrice[shopType][city]
                ConfigData.stations[shopType][city] = CraftStation(fee_ = fee, city_ = city)
            # For each item --> craftNode, do the following:
            for craftNodeLine in self.cnSetup[shopType]:

                # Fetch the list of valid nums and create a Spec for each string
                allSpecs = []
                for specItem in craftNodeLine[1]:
                    validNums = ConfigData.GetAllValidNums(itemLines, specItem)
                    newSpec = Spec(validItems_ = validNums, specName_ = str(specItem))
                    allSpecs.append(newSpec)

                # Get aggregate valid items
                allValidItems = []
                for spec in allSpecs:
                    allValidItems += spec.validItems

                # Create a CraftNode and add it to weaponTypes for each city, also valid ids/nums
                newCraftNode = CraftNode(name_ = craftNodeLine[0], specs_=allSpecs, validItems_=allValidItems)
                for city in ConfigData.cities:
                    ConfigData.stations[shopType][city].weaponTypes.append(newCraftNode)
                    ConfigData.stations[shopType][city].validIds += allValidItems

        itemfile.close()

    @staticmethod
    def GetAllValidNums(lineListIn, strListIn):
        MIN_ITEMNUM = 1276  # discard if below
        numList = []
        for line in lineListIn:
            if ConfigData.findAll(line, strListIn) is True:
                num = line.split(":")[0]
                if num > MIN_ITEMNUM:
                    numList.append(num)
        return numList

    @staticmethod
    def findAll(lineIn, strList):
        for param in strList:
            if lineIn.find(param) == -1:
                return False
        # finished, return
        return True

    def SetupCraftStations(self):
        #TODOne: load crafting stations and fees from file --> in different method above
        #TODO: load masteries and specs from file
        pass

    @staticmethod
    def GetCraftStation(itemNum_, city):
        #TODOne: Get crafting station based on an item num and city
        for shopType in ConfigData.shopTypes:
            if ConfigData.stations[shopType][city].IsNumInStation(itemNum_):
                return ConfigData.stations[shopType][city]

        print("WARN: GetCraftStation({}, {}) returned None".format(itemNum_, city))
        return None

class CraftStation(object):
    def __init__(self, validIds_ = [], fee_ = 0.0, weaponTypes_ = [], city_ = ""):
        self.validIds = validIds_
        self.fee = fee_
        self.weaponTypes = weaponTypes_
        self.city = city_

    def ReturnRate(self, city_, usingFocus_):
        #TODO: Calculate return rate using city, focus
        pass

    def DoCraft(self, toCraft_):
        #TODO: return CraftResult with results of potential craft
        pass

    def IsNumInStation(self, num):
        if num in self.validIds: return True
        else: return False

class CraftNode(object):
    def __init__(self, specs_ = [], level_ = 0, validItems_ = [], name_ = ""):
        self.specs = specs_
        self.level = level_
        self.validItems = validItems_
        self.name = name_

    def GetFocusEfficiency(self, spec):
        #TODO: return int, calculate Focus efficiency value:
        """
        focusCost = baseFocus * focusMulti
            BaseFocus=(1.75^LOG(ItemValue, 2))*10
            FocusUsed=BaseFocus*(0.5^(FocusEfficiency/100))
                FocusEfficiency = 30*CraftNode.level + 250*Spec.level
        """
        pass

class Spec(object):
    def __init__(self, validItems_ = [], level_ = 0, specName_ = ""):
        self.validItems = validItems_
        self.level = level_
        self.specName = specName_

class ItemQualClass(object):
    def __init__(self, items_ = {}):
        self.items = items_
        self.craftData = CraftData(self)

    def GetItem(self, quality):
        if quality in self.items:
            return self.items[quality]
        else:
            print("WARN: Spec.GetItem returned 'None' : {}".format(quality))
            return None

    def FetchQualHistory(self):
        #TODO: Fetch price history for a quality group. Insert those into the PriceData (new PriceRecords).
        pass

    def CraftItem(self, city):
        #TODO: Craft an item quality class in a city and return the corresponding CraftResult
        # Calls next on the craftdata to call the crafting station
        pass

class Item(object):
    def __init__(self, id_, name_, quality_):
        self.id = id_
        self.name = name_
        self.enchant = self.GenerateEnchant()
        self.quality = quality_
        self.baseItemValue = None
        self.priceData = {}

    def DoTrade(self, city):
        #TODO: Return a TradeResult for the specified city, detailing the buy/sell locations, prices, and profit.
        pass

    def DoAllTrades(self, cities):
        #TODO: Do a DoTrade() for each city in the specified list of cities, return a list of TradeResults
        pass

    def GenerateEnchant(self):
        split = self.id.split("@")
        if len(split) > 1:
            return split[1]
        else:
            return 0

class PriceData(object):
    defaultTimeString = "0001-01-01T00:00:00"
    def __init__(self, city_):
        self.city = city_
        self.prices = [0,0,0,0]
        self.dates = [PriceData.defaultTimeString] * 4
        self.history = []

    def GetMargin(self):
        #TODO: Return price margin
        pass

    def GetSellMin(self):
        return self.prices[SELL_MIN]

    def GetBuyMax(self):
        return self.prices[BUY_MAX]

    def SetSellMin(self, p_):
        self.prices[SELL_MIN] = p_

    def SetBuyMax(self, s_):
        self.prices[BUY_MAX] = s_

    def GetHistoricalPrice(self):
        #TODO: Get an average price over a reasonably long time domain
        # Ex., get an average weighted by the number of sold per price, over the last (1?) month
        pass

class PriceRecord(object):
    def __init__(self, city_, quantity_, price_, time_):
        self.city = city_
        self.quantity = quantity_
        self.price = price_
        self.time = time_

class CraftResult(object):
    def __init__(self, station_, focusCost_, buyCost_, sellCost_, profit_):
        self.station = station_
        self.focusCost = focusCost_
        self.buyCost = buyCost_
        self.sellCost = sellCost_
        self.profit = profit_

    def __str__(self):
        #TODO: return string representation of craft, like original Item's
        return ""

class TradeResult(object):
    def __init__(self, item_, buyLocation_, sellLocation_, buyPrice_, sellPrice_, profit_):
        self.item = item_
        self.buyLocation = buyLocation_
        self.sellLocation = sellLocation_
        self.buyPrice = buyPrice_
        self.sellPrice = sellPrice_
        self.profit = profit_

    def __str__(self):
        # TODO: return string representation of trade, like original Item's
        return ""

class CraftData(object):
    def __init__(self, itemQC_):
        self.materials = []
        self.stations = []
        self.itemQC = itemQC_

    def DoCraft(self, city, itemToCraft):
        #TODO: Return CraftResult object. calls next on the crafting station
        pass


